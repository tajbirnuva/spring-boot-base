package com.example.springbase.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DepartmentDto {
    private long departmentId;
    private String departmentName;
    private String departmentCode;
    private List<StudentDto> studentList;
    private List<SubjectDto> subjectList;

    private List<Long> studentIdList;
}
