package com.example.springbase.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmailDto {

    private String emailTo;
    private String emailSubject;
    private String emailBody;

}
