package com.example.springbase.service;

import com.example.springbase.entity.Department;
import com.example.springbase.repository.DepartmentRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DepartmentService {
    private final DepartmentRepo departmentRepo;

    public DepartmentService(DepartmentRepo departmentRepo) {
        this.departmentRepo = departmentRepo;
    }


    public void save(Department department) {
        departmentRepo.save(department);
    }


    public List<Department> getAllDepartments() {
        return departmentRepo.findAll();
    }


    public Department getDepartment(long departmentId) {
        return departmentRepo.getOne(departmentId);
    }

}
