package com.example.springbase.service;

import com.example.springbase.entity.Subject;
import com.example.springbase.repository.SubjectRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubjectService {
    private final SubjectRepo subjectRepo;

    public SubjectService(SubjectRepo subjectRepo) {
        this.subjectRepo = subjectRepo;
    }


    public void saveSubject(Subject subject) {
        subjectRepo.save(subject);
    }


    public List<Subject> getAllSubject() {
        return subjectRepo.findAll();
    }


    public Subject getSubjectById(long subjectId) {
        return subjectRepo.getOne(subjectId);
    }

}
