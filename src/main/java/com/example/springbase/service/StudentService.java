package com.example.springbase.service;

import com.example.springbase.entity.Student;
import com.example.springbase.repository.StudentRepo;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    private final StudentRepo studentRepo;

    public StudentService(StudentRepo studentRepo) {
        this.studentRepo = studentRepo;
    }


    public void saveStudent(Student student) {
        studentRepo.save(student);
    }


    public List<Student> getAllStudents() {
        return studentRepo.findAllByEnableTrue();
    }


    public Student getStudentById(long studentId) {
        return studentRepo.getOne(studentId);
    }

}
