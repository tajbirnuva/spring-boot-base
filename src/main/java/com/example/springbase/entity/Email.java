package com.example.springbase.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "email")
@Getter
@Setter
public class Email{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long emailId;
    @Column
    private String emailTo;
    @Column
    private String emailSubject;
    @Column(length = 1000)
    private String emailBody;
}
