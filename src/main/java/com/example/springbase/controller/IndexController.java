package com.example.springbase.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @RequestMapping("/")
    public String welcome(Model model) {
        model.addAttribute("title", "Welcome");
        return "/welcome";
    }


    @GetMapping("/get-login")
    public String getLoginPage() {
        return "/sign-in";
    }

}
