package com.example.springbase.controller;

import com.example.springbase.dto.DepartmentDto;
import com.example.springbase.dto.SubjectDto;
import com.example.springbase.entity.Department;
import com.example.springbase.entity.Subject;
import com.example.springbase.service.DepartmentService;
import com.example.springbase.service.SubjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/subject")
public class SubjectController {
    private final SubjectService subjectService;
    private final DepartmentService departmentService;

    public SubjectController(SubjectService subjectService, DepartmentService departmentService) {
        this.subjectService = subjectService;
        this.departmentService = departmentService;
    }


    @GetMapping("/add")
    public String getSubjectAddPage(Model model) {
        model.addAttribute("title", "Add New Subject");
        model.addAttribute("departmentDtoList", this.getDepartmentDtoList());
        return "subject/add-show";
    }


    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubjectDto saveSubject(@RequestBody SubjectDto subjectDto) {
        Subject subject = new Subject();
        BeanUtils.copyProperties(subjectDto, subject);
        subject.setDepartmentList(this.getDepartments(subjectDto.getDepartmentIdList()));
        subjectService.saveSubject(subject);
        return subjectDto;
    }


    @GetMapping(value = "/getAllSubject", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SubjectDto> getAllSubject() {
        List<Subject> subjectList = subjectService.getAllSubject();
        List<SubjectDto> subjectDtoList = this.getSubjectDtoList(subjectList);
        return subjectDtoList;
    }


    /*--------------------------Helper Method------------------------------*/

    private List<DepartmentDto> getDepartmentDtoList() {
        List<Department> departmentList = departmentService.getAllDepartments();
        List<DepartmentDto> departmentDtoList = new ArrayList<>();
        for (Department department : departmentList) {
            DepartmentDto departmentDto = new DepartmentDto();
            BeanUtils.copyProperties(department, departmentDto);
            departmentDtoList.add(departmentDto);
        }
        return departmentDtoList;
    }


    private List<Department> getDepartments(List<Long> departmentIdList) {
        List<Department> departmentList = new ArrayList<>();
        for (long departmentId : departmentIdList) {
            Department department = departmentService.getDepartment(departmentId);
            departmentList.add(department);
        }
        return departmentList;
    }


    private List<SubjectDto> getSubjectDtoList(List<Subject> subjectList) {
        List<SubjectDto> subjectDtoList = new ArrayList<>();
        for (Subject subject : subjectList) {
            SubjectDto subjectDto = new SubjectDto();
            BeanUtils.copyProperties(subject, subjectDto);
            subjectDtoList.add(subjectDto);
        }
        return subjectDtoList;
    }

}
