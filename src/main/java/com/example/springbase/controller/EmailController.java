package com.example.springbase.controller;

import com.example.springbase.dto.EmailDto;
import com.example.springbase.entity.Email;
import com.example.springbase.service.EmailService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("/email")
public class EmailController {
private final EmailService emailService;

    public EmailController(EmailService emailService) {
        this.emailService = emailService;
    }


    @GetMapping("/email-send")
    public String getEmailSendPage(Model model) {
        model.addAttribute("title", "Send Email");
        model.addAttribute("emailDto", new EmailDto());
        return "email/send-email";
    }


    @PostMapping("/send")
    public String sendEmail(@ModelAttribute EmailDto emailDto, RedirectAttributes redirectAttributes) {
        Email email = new Email();
        BeanUtils.copyProperties(emailDto, email);
        emailService.sendEmail(email);
        redirectAttributes.addFlashAttribute("message","Email Send Successfully");
        return "redirect:/email/email-send";
    }


}
