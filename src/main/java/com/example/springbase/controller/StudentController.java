package com.example.springbase.controller;

import com.example.springbase.dto.DepartmentDto;
import com.example.springbase.dto.StudentDto;
import com.example.springbase.entity.Department;
import com.example.springbase.entity.Student;
import com.example.springbase.service.StudentService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {
    private final StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping("/add")
    public String getStudentAddPage(Model model) {
        model.addAttribute("title", "Add New Student");
        model.addAttribute("studentDto", new StudentDto());
        model.addAttribute("genderList", this.genderList());
        return "student/add";
    }


    @PostMapping("/save")
    public String saveStudent(@ModelAttribute StudentDto studentDto) {
        Student student;
        if (studentDto.getStudentId() != 0) {
            student=studentService.getStudentById(studentDto.getStudentId());
        }else {
            student=new Student();
        }
        BeanUtils.copyProperties(studentDto, student);
        studentService.saveStudent(student);
        return "redirect:/student/getAll";
    }


    @GetMapping("/getAll")
    public String getAllStudents(Model model) {
        model.addAttribute("title","All Students");
        List<Student> studentList = studentService.getAllStudents();
        List<StudentDto> studentDtoList = this.getStudentDtoList(studentList);
        model.addAttribute("studentDtoList", studentDtoList);
        return "student/get-all";
    }


    @GetMapping("/update/{studentId}")
    public String updateStudent(@PathVariable(name = "studentId") long studentId, Model model) {
        StudentDto studentDto = new StudentDto();
        Student student = studentService.getStudentById(studentId);
        BeanUtils.copyProperties(student, studentDto);
        model.addAttribute("studentDto", studentDto);
        model.addAttribute("genderList", this.genderList());
        return "student/add";
    }


    @GetMapping("/delete/{studentId}")
    public String deleteStudent(@PathVariable(name = "studentId") long studentId) {
        Student student=studentService.getStudentById(studentId);
        student.setEnable(false);
        studentService.saveStudent(student);
        return "redirect:/student/getAll";
    }


    /*---------------------------- Helper Method---------------------------------*/

    private List<String> genderList() {
        List<String> genderList = new ArrayList<>();
        genderList.add("Male");
        genderList.add("Female");
        return genderList;
    }


    private List<StudentDto> getStudentDtoList(List<Student> studentList) {
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student : studentList) {
            StudentDto studentDto = new StudentDto();
            BeanUtils.copyProperties(student, studentDto);
            if (student.getDepartment() == null) {
                DepartmentDto departmentDto = new DepartmentDto();
                departmentDto.setDepartmentCode("not Assign Yet");
                studentDto.setDepartmentDto(departmentDto);
            } else {
                studentDto.setDepartmentDto(getDepartmentDto(student));
            }
            studentDtoList.add(studentDto);
        }
        return studentDtoList;
    }


    private DepartmentDto getDepartmentDto(Student student) {
        DepartmentDto departmentDto = new DepartmentDto();
        BeanUtils.copyProperties(student.getDepartment(), departmentDto);
        return departmentDto;
    }

}
