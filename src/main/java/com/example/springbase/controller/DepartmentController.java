package com.example.springbase.controller;

import com.example.springbase.dto.DepartmentDto;
import com.example.springbase.dto.StudentDto;
import com.example.springbase.dto.SubjectDto;
import com.example.springbase.entity.Department;
import com.example.springbase.entity.Student;
import com.example.springbase.entity.Subject;
import com.example.springbase.service.DepartmentService;
import com.example.springbase.service.StudentService;
import com.example.springbase.service.SubjectService;
import org.springframework.beans.BeanUtils;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/department")
public class DepartmentController {
    private final DepartmentService departmentService;
    private final StudentService studentService;
    private final SubjectService subjectService;

    public DepartmentController(DepartmentService departmentService, StudentService studentService, SubjectService subjectService) {
        this.departmentService = departmentService;
        this.studentService = studentService;
        this.subjectService = subjectService;
    }


    @GetMapping("/add")
    public String getDepartmentAddPage(Model model) {
        model.addAttribute("title", "Department Add & Show");
        model.addAttribute("departmentDto", new DepartmentDto());
        model.addAttribute("departmentDtoList", this.getDepartmentDtoList());
        return "department/add-show";
    }


    @PostMapping("/save")
    public String saveDepartment(@ModelAttribute DepartmentDto departmentDto) {
        Department department = new Department();
        BeanUtils.copyProperties(departmentDto, department);
        departmentService.save(department);
        return "redirect:/department/add";
    }


    @GetMapping("/assign-student")
    public String getStudentAssignPage(Model model) {
        model.addAttribute("title", "Assign Student In Department");
        model.addAttribute("departmentDto", new DepartmentDto());
        model.addAttribute("departmentDtoList", this.getDepartmentDtoList());
        model.addAttribute("studentDtoList", this.getNonAssignStudentList());
        return "department/assign-student";
    }


    @PostMapping("/assign")
    public String assignStudent(@ModelAttribute DepartmentDto departmentDto) {
        Department department = departmentService.getDepartment(departmentDto.getDepartmentId());
        department.setStudentList(this.getStudents(departmentDto.getStudentIdList()));
        departmentService.save(department);
        return "redirect:/department/assign-student";
    }


    @GetMapping("/get-details")
    public String getDepartmentDetailPage(Model model) {
        model.addAttribute("title", "Department's Student & Subject Details");
        model.addAttribute("departmentDtoList", this.getDepartmentDtoList());
        return "department/get-students-subjects";
    }


    @GetMapping(value = "/get-students-subjects-details/{departmentId}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DepartmentDto getStudentsSubjectsByDept(@PathVariable("departmentId") long departmentId) {
        Department department = departmentService.getDepartment(departmentId);
        return getDepartmentDto(department);
    }


    @GetMapping(value = "/get-student/{studentId}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudentDto getStudent(@PathVariable("studentId") long studentId) {
        Student student = studentService.getStudentById(studentId);
        StudentDto studentDto = new StudentDto();
        BeanUtils.copyProperties(student, studentDto);
        return studentDto;
    }


    @GetMapping(value = "/get-subject/{subjectId}",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubjectDto getSubject(@PathVariable("subjectId") long subjectId) {
        Subject subject = subjectService.getSubjectById(subjectId);
        SubjectDto subjectDto = new SubjectDto();
        BeanUtils.copyProperties(subject, subjectDto);
        return subjectDto;
    }


    /*--------------------------Helper Method------------------------------*/

    private List<DepartmentDto> getDepartmentDtoList() {
        List<Department> departmentList = departmentService.getAllDepartments();
        List<DepartmentDto> departmentDtoList = new ArrayList<>();
        for (Department department : departmentList) {
            DepartmentDto departmentDto = new DepartmentDto();
            BeanUtils.copyProperties(department, departmentDto);
            departmentDtoList.add(departmentDto);
        }
        return departmentDtoList;
    }


    private List<StudentDto> getNonAssignStudentList() {
        List<Student> studentList = studentService.getAllStudents();
        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student : studentList) {
            if (student.getDepartment() == null) {
                StudentDto studentDto = new StudentDto();
                BeanUtils.copyProperties(student, studentDto);
                studentDtoList.add(studentDto);
            }
        }
        return studentDtoList;
    }


    private List<Student> getStudents(List<Long> studentIdList) {
        List<Student> studentList = new ArrayList<>();
        for (long studentId : studentIdList) {
            Student student = studentService.getStudentById(studentId);
            studentList.add(student);
        }
        return studentList;
    }


    private DepartmentDto getDepartmentDto(Department department) {
        DepartmentDto departmentDto = new DepartmentDto();
        BeanUtils.copyProperties(department, departmentDto);

        List<StudentDto> studentDtoList = new ArrayList<>();
        for (Student student : department.getStudentList()) {
            StudentDto studentDto = new StudentDto();
            BeanUtils.copyProperties(student, studentDto);
            studentDtoList.add(studentDto);
        }
        departmentDto.setStudentList(studentDtoList);

        List<SubjectDto> subjectDtoList = new ArrayList<>();
        for (Subject subject : department.getSubjectList()) {
            SubjectDto subjectDto = new SubjectDto();
            BeanUtils.copyProperties(subject, subjectDto);
            subjectDtoList.add(subjectDto);
        }
        departmentDto.setSubjectList(subjectDtoList);

        return departmentDto;
    }

}
