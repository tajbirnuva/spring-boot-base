package com.example.springbase.init;

import com.example.springbase.entity.Role;
import com.example.springbase.entity.User;
import com.example.springbase.repository.RoleRepo;
import com.example.springbase.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DataInitializer implements CommandLineRunner {
    @Autowired
    RoleRepo roleRepo;
    @Autowired
    UserService userService;

    @Override
    public void run(String... args) throws Exception {

        List<Role> roleList = Arrays.asList(
                new Role("STUDENT"),
                new Role("DEPARTMENT"),
                new Role("SUBJECT")
        );

        /*roleRepo.saveAll(roleList);*/

        /*-----------------------NEW USERS WITH THEIR ROLE----------------------*/
        List<Role> roleList1 = Arrays.asList(roleRepo.getOne(1L));
        List<Role> roleList2 = Arrays.asList(roleRepo.getOne(2L));
        List<Role> roleList3 = Arrays.asList(roleRepo.getOne(3L));


        List<User> userList = Arrays.asList(
                new User("student@gmail.com", "12345", roleList1),
                new User("department@gmail.com", "12345", roleList2),
                new User("subject@gmail.com", "12345", roleList3)
        );

        /*userService.saveAll(userList);*/

    }
}
